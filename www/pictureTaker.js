/**
 * cordova is available under the MIT License (2008).
 * See http://opensource.org/licenses/alphabetical for full text.
 *
 * Copyright (c) Matt Kane 2010
 * Copyright (c) 2011, IBM Corporation
 * Copyright (c) 2012-2017, Adobe Systems
 */


var exec = cordova.require("cordova/exec");

var takeInProgress = false;

/**
 * Constructor.
 *
 * @returns {PictureTaker}
 */
function PictureTaker() {

    /**
     * Encoding constants.
     *
     * @type Object
     */
    this.Encode = {
        TEXT_TYPE: "TEXT_TYPE",
        EMAIL_TYPE: "EMAIL_TYPE",
        PHONE_TYPE: "PHONE_TYPE",
        SMS_TYPE: "SMS_TYPE"
        //  CONTACT_TYPE: "CONTACT_TYPE",  // TODO:  not implemented, requires passing a Bundle class from Javascript to Java
        //  LOCATION_TYPE: "LOCATION_TYPE" // TODO:  not implemented, requires passing a Bundle class from Javascript to Java
    };
}

/**
* Read code from scanner.
*
* @param {Function} successCallback This function will recieve a result object: {
 *        text : '12345-mock',    // The code that was scanned.
 *        format : 'FORMAT_NAME', // Code format.
 *        cancelled : true/false, // Was canceled.
 *    }
* @param {Function} errorCallback
* @param options
*/
PictureTaker.prototype.take = function (successCallback, errorCallback, options) {

    if (options instanceof Array) {
        // do nothing
    } else {
        options = [];
    }

    if (errorCallback == null) {
        errorCallback = function () {
        };
    }

    if (typeof errorCallback != "function") {
        console.log("PictureTaker.take failure: failure parameter not a function");
        return;
    }

    if (typeof successCallback != "function") {
        console.log("PictureTaker.take failure: success callback parameter must be a function");
        return;
    }

    // if (takeInProgress) {
    //     errorCallback('take is already in progress');
    //     return;
    // }

    takeInProgress = true;

    exec(
        function(result) {
            takeInProgress = false;
            // work around bug in ZXing library
            successCallback(result);
        },
        function(error) {
            takeInProgress = false;
            errorCallback(error);
        },
        'CDVPictureTaker',
        'take',
        options
    );
};

PictureTaker.prototype.detectFaceAndTake = function (successCallback, errorCallback, options) {

    if (options instanceof Array) {
        // do nothing
    } else {
        options = [];
    }

    if (errorCallback == null) {
        errorCallback = function () {
        };
    }

    if (typeof errorCallback != "function") {
        console.log("PictureTaker.take failure: failure parameter not a function");
        return;
    }

    if (typeof successCallback != "function") {
        console.log("PictureTaker.take failure: success callback parameter must be a function");
        return;
    }

    // if (takeInProgress) {
    //     errorCallback('take is already in progress');
    //     return;
    // }

    takeInProgress = true;

    exec(
        function(result) {
            takeInProgress = false;
            // work around bug in ZXing library
            successCallback(result);
        },
        function(error) {
            takeInProgress = false;
            errorCallback(error);
        },
        'CDVPictureTaker',
        'detectFaceAndTake',
        options
    );
};

var pictureTaker = new PictureTaker();
module.exports = pictureTaker;
